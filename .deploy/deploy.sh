#!/bin/bash

# Unpack
tar -xvzf nowak_projekt.tar.gz

# Install dependencies
cd bundle/programs/server/
npm i --production
cd ../..

# Create run.sh file
echo '#!/bin/bash' > run.sh
echo "export MONGO_URL=mongodb://grupa1:grupa1@ds217138.mlab.com:17138/grupa1?socketTimeoutMS=30000" >> run.sh
echo "export ROOT_URL=http://192.168.43.232" >> run.sh
echo "export PORT=3000" >> run.sh
echo "" >> run.sh
echo "forever start --uid "Projekt" -a -l /var/log/projekt.log main.js" >> run.sh
chmod +x run.sh
