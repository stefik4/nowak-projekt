export default {
    isUserLoggedIn: () => {
        if (Meteor.user() && FlowRouter.current().path === '/login') {
            FlowRouter.go('/');
        } else if (!Meteor.user() && FlowRouter.current().path === '/') {
            FlowRouter.go('/login');
        }
    }
}
