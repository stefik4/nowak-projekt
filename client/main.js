import React from 'react';
import { Meteor } from 'meteor/meteor';

import '../imports/startup/index';
import './routes';

import LocalStorageProvider from '/imports/libs/LocalStorageProvider';

Meteor.subscribe('devices');
Meteor.subscribe('myStatus');
Meteor.subscribe('data');
Meteor.subscribe('dataTypes');

Meteor.startup(() => {
});

window.LocalStorageProvider = new LocalStorageProvider();
