import React from 'react';
import { mount } from 'react-mounter';

import HomePage from '/imports/ui/HomePage';
import AppWrapper from '/imports/ui/AppWrapper';

FlowRouter.route('/', {
    name: 'home',
    action() {
        mount(AppWrapper, {
            main: <HomePage/>
        });
    }
});
