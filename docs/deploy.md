Jak uruchomić aplikację?

Wymagania:
- Node.js >=8
- forever (npm i -g forever)
- Ustawić poprawny adres maszyny w pliku deploy.sh w lini 14

Proces:
- W folderze .deploy znajduje się zbudowana paczka
- cd .deploy/
- Należy uruchomić skrypt bashowy: sudo sh deploy.sh (sudo poniewaz musi miec dostep do zapisu plikow)
    - Skrypt rozpakowywuje paczke
    - Instaluje node_modules
    - Ustawia zmienne srodowiskowe
    - Tworzy plik uruchomieniowy
    
- cd bundle
- sudo sh run.sh (sudo poniewaz logi zapisywane sa w sciezce absolutnej)
    - uruchomienie projektu
    - aby zatrzymac (sudo forever stopall)

