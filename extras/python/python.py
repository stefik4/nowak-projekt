from flask import Flask, render_template, request
from pymongo import MongoClient
import datetime

app = Flask(__name__)


@app.route('/temp', methods=['POST'])
def temp():
    connection_params = {
        'user': 'grupa1',
        'password': 'grupa1',
        'host': 'ds217138.mlab.com',
        'port': 17138,
        'namespace': 'grupa1',
    }

    client = MongoClient(
        'mongodb://{user}:{password}@{host}:'
        '{port}/{namespace}'.format(**connection_params)
    )
    db = client.grupa1
    data = db.data
    data_types = db.dataTypes
    devices = db.devices
    request_data = request.get_json()

    now = datetime.datetime.now()

    device = devices.find_one({'ip': request.remote_addr})
    if not device:
        new_device = {
            'name': request_data["deviceName"],
            'ip': request.remote_addr,
            'addedAt': str(now),
            'addedBy': ''
        }
        devices.insert_one(new_device)

    measurement = {'type': request_data["typeKey"], 'value': request_data["value"], 'source': request.remote_addr, 'date': str(now)}
    data.insert_one(measurement)

    data_type = data_types.find_one({'key': request_data["typeKey"]})
    if not data_type:
        new_data_type = {
            'key': request_data["typeKey"],
            'name': request_data["typeName"]
        }
        data_types.insert_one(new_data_type)

    now = datetime.datetime.now()
    request_data = request.get_json()
    print(request_data)
    print({'type': request_data["typeKey"], 'value': request_data["value"], 'source': request.remote_addr, 'date': str(now)})
    return render_template('/index.html')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
