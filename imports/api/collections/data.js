import { Mongo } from 'meteor/mongo';
import { options } from './driver';

const data = new Mongo.Collection('data', options);

data.allow({
    insert: () => true,
    update: () => true,
    remove: () => true
});

export default data;
