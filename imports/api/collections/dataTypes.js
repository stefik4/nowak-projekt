import { Mongo } from 'meteor/mongo';
import { options } from './driver';

const dataTypes = new Mongo.Collection('dataTypes', options);

dataTypes.allow({
    insert: () => true,
    update: () => true,
    remove: () => true
});

export default dataTypes;
