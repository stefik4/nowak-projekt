import { Mongo } from 'meteor/mongo';
import { options } from './driver';

const devices = new Mongo.Collection('devices', options);

export default devices;
