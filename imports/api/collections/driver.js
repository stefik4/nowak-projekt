const isAllowedMultipleDatabases = true;

let options;
if (Meteor.isServer && isAllowedMultipleDatabases) {
    const mongoAddress = 'mongodb://grupa1:grupa1@ds217138.mlab.com:17138/grupa1?socketTimeoutMS=30000';
    const secondDatabase = new MongoInternals.RemoteCollectionDriver(mongoAddress);
    options = { _driver: secondDatabase };
} else {
    options = {};
}

export {
    options
};
