import { check } from 'meteor/check';
import DataTypes from '../collections/dataTypes';

Meteor.methods({
    'removeDataType'(typeId) {
        DataTypes.remove({ _id: typeId });
    },
    'editDataTypeName'(typeId, typeName) {
        DataTypes.update({ _id: typeId }, { $set: { name: typeName } });
    }
});
