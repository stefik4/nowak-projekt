import { check, Match } from 'meteor/check';
import Devices from '../collections/devices';

Meteor.methods({
    'removeDevice'(deviceId) {
        Devices.remove({ _id: deviceId });
    },
    'editDeviceName'(deviceId, deviceName) {
        Devices.update({ _id: deviceId }, { $set: { name: deviceName } });
    }
});
