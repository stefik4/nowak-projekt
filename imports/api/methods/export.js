import XLSX from 'xlsx';
import Data from './../collections/data';

Meteor.methods({
    'download'(selector) {
        let document;
        // const elements = Data.find().fetch();
        const elements = [['ID rekordu', 'Data','Zrodlo', 'Klucz','Wartosc']];
        Data.find(selector).fetch().forEach((item) => {
            const tmp = [];
            Object.keys(item).forEach((e) => {
                tmp.push(item[e]);
            });
            elements.push(tmp);
        });
        const data = XLSX.utils.aoa_to_sheet(elements);
        document = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(document, data, 'SheetJS');
        return document;
    }
});
