import { Meteor } from 'meteor/meteor';
import DataTypes from '../collections/dataTypes';

if (Meteor.isServer) {
    Meteor.publish('dataTypes', function publication() {
        if (this.userId) {
            return DataTypes.find();
        }
        return this.ready();
    });
}
