import { Meteor } from 'meteor/meteor';
import Devices from '../collections/devices';

if (Meteor.isServer) {
    Meteor.publish('devices', function publication() {
        if (this.userId) {
            return Devices.find();
        }
        return this.ready();
    });
}
