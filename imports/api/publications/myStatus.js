import { Meteor } from 'meteor/meteor';

if (Meteor.isServer) {
    Meteor.publish('myStatus', function publication() {
        if (this.userId) {
            return Meteor.users.find(this.userId);
        }
        return this.ready();
    });
}
