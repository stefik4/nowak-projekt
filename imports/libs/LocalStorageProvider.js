export default class LocalStorageProvider {
    constructor() {
        this.storage = window.localStorage;
    }

    set(name, value) {
        this.storage.setItem(name, value);
    }

    get(name) {
        return this.storage.getItem(name);
    }
}
