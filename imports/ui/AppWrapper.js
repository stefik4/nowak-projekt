import { withTracker } from 'meteor/react-meteor-data';

const AppWrapper = props => props.main;

export default withTracker(() => ({}))(AppWrapper);
