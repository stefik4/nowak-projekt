import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import { PageHeader } from 'react-bootstrap';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { withTracker } from 'meteor/react-meteor-data';
import { ResponsiveContainer, LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import Data from './../api/collections/data';
import DataTypes from './../api/collections/dataTypes';
import Devices from './../api/collections/devices';

const Charts = class extends React.Component {
    constructor() {
        super();
        this.state = {
            limit: 100
        };
    }
    render() {
        return (
            <div>
                <PageHeader>
                    Konfiguracja <small>Zdefiniuj parametry wykresu</small>
                </PageHeader>
                <Select
                    name="form-field-name"
                    value={this.state.limit}
                    onChange={(event) => this.setState({ limit: event.value })}
                    options={[
                        { value: 100, label: '100' },
                        { value: 200, label: '200' },
                        { value: 300, label: '300' },
                        { value: 400, label: '400' },
                        { value: 500, label: '500' },
                    ]}
                />
                <PageHeader>
                    Wykres <small>Dane zaprezentowane z określonymi parametrami</small>
                </PageHeader>
                {
                    this.props.data.map((dataType) => {
                        return dataType.devices.map((device, key) => {
                            if (device.items.length > 0) {
                                return (
                                    <div key={key} style={{height: '20vw', marginBottom: '7em'}}>
                                        <h3>{dataType.name} - {device.deviceName} ({device.ip}) [ilość pomiarów: {device.items.length}]</h3>
                                        <ResponsiveContainer width='100%' height="100%">
                                            <LineChart data={_.takeRight(device.items, this.state.limit)} margin={{top: 15, right: 0, left: 0, bottom: 0}}>
                                                <XAxis dataKey="name"/>
                                                <YAxis yAxisId="left"/>
                                                <YAxis yAxisId="right" orientation="right"/>
                                                <CartesianGrid strokeDasharray="3 3"/>
                                                <Tooltip/>
                                                {/*<Legend/>*/}
                                                <Line yAxisId="right" type="monotone" dataKey='value' stroke="#82ca9d" activeDot={{r: 8}}/>
                                            </LineChart>
                                        </ResponsiveContainer>
                                    </div>
                                );
                            }
                        });
                    })
                }
            </div>
        );
    }
};

export default withTracker(() => {
    const dataTypesCount = DataTypes.find().count();
    let data = [];
    if (dataTypesCount > 0) {
        DataTypes.find().fetch().forEach((dataType) => {
            const { name, key } = dataType;
            const devices = [];
            Devices.find().fetch().forEach((device) => {
                const { name, ip } = device;
                const items = Data.find({ type: key, source: ip }, { sort: { date: 1 } } ).map((item) => {
                    const { date, value } = item;
                    return {
                        name: moment(date).format('DD.MM HH:mm:ss'),
                        value
                    };
                });
                devices.push({
                    deviceName: name,
                    ip,
                    items
                });
            });
            data.push({
                key,
                name,
                devices
            })
        });
        return { data };
    }
    return {
        data
    };
})(Charts);
