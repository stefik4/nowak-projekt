import React from 'react';
import { Table, Button, FormControl, Alert, ControlLabel, Modal, ButtonToolbar } from 'react-bootstrap';
import { withTracker } from 'meteor/react-meteor-data';

import DataTypes from './../api/collections/dataTypes';

class Devices extends React.Component {
    constructor() {
        super();
        this.state = {
            modal: false,
            selectedDataTypeId: null,
            dataTypeName: ''
        };
    }
    editDataTypeName() {
        Meteor.call('editDataTypeName', this.state.selectedDataTypeId, this.state.dataTypeName);
        this.setState({ modal: false });
    }
    removeDataType(typeId) {
        Meteor.call('removeDataType', typeId);
    }
    renderModal() {
        return (
            <Modal show={this.state.modal}>
                <Modal.Header>
                    <Modal.Title>Zmiana nazwy typu</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ControlLabel>Nowa nazwa:</ControlLabel>
                    <FormControl
                        type="text"
                        placeholder="Wprowadź nazwę typu"
                        onChange={(e) => this.setState({ dataTypeName: e.target.value })}
                    />
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        bsStyle="success"
                        onClick={this.editDataTypeName.bind(this)}
                    >Zapisz</Button>
                </Modal.Footer>
            </Modal>
        );
    }
    renderTable() {
        return this.props.dataTypes.map((item, index) => (
            <tr key={index}>
                <td>{(index + 1)}.</td>
                <td>{item.name}</td>
                <td>{item.key}</td>
                <td>
                    {
                        this.props.canEdit ?
                            <ButtonToolbar>
                                <Button bsSize="xsmall" bsStyle="info" onClick={() => this.setState({ modal: true, selectedDataTypeId: item._id })}>Zmień nazwę</Button>
                                <Button bsSize="xsmall" bsStyle="danger" onClick={this.removeDataType.bind(null, item._id)}>Usuń</Button>
                            </ButtonToolbar>
                            :
                            null
                    }
                </td>
            </tr>
        ));
    }
    render() {
        return (
            <div>
                {this.renderModal()}
                <h1>Lista typów danych</h1>
                <Alert bsStyle="warning">
                    <strong>Informacja!</strong> Aby dane z urządzenia wyświetały się na wykresach, typ danych musi być dodany na poniższej liście z odpowiednim kluczem, który definiowany jest w nadajniku ESP8266.
                </Alert>
                <Alert bsStyle="info">
                    <strong>Informacja!</strong> Nierozpoznane typy danych zostaną automatycznie dodane do tej listy, po przesłaniu pierwszego nierozpoznanego pakietu danych.
                </Alert>
                <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        <th>#.</th>
                        <th>Nazwa</th>
                        <th>Klucz</th>
                        <th>Akcje</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderTable()}
                    </tbody>
                </Table>
            </div>
        );
    }
}


export default withTracker(() => {
    const canEdit = (Meteor.user() && Meteor.user().role === 'admin');
    return {
        dataTypes: DataTypes.find().fetch(),
        canEdit
    }
})(Devices);
