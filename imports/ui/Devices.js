import React from 'react';
import { Table, Button, Alert, ButtonToolbar, Modal, ControlLabel, FormControl } from 'react-bootstrap';
import { withTracker } from 'meteor/react-meteor-data';

import DevicesCollection from './../api/collections/devices';

class Devices extends React.Component {
    constructor() {
        super();
        this.state = {
            modal: false,
            selectedDeviceId: null,
            deviceName: ''
        };
    }
    editDeviceName() {
        Meteor.call('editDeviceName', this.state.selectedDeviceId, this.state.deviceName);
        this.setState({ modal: false });
    }
    removeDevice(deviceId) {
        Meteor.call('removeDevice', deviceId);
    }
    renderModal() {
        return (
            <Modal show={this.state.modal}>
                <Modal.Header>
                    <Modal.Title>Zmiana nazwy urządzenia</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ControlLabel>Nowa nazwa:</ControlLabel>
                    <FormControl
                        type="text"
                        placeholder="Wprowadź nazwę urządzenia"
                        onChange={(e) => this.setState({ deviceName: e.target.value })}
                    />
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        bsStyle="success"
                        onClick={this.editDeviceName.bind(this)}
                    >Zapisz</Button>
                </Modal.Footer>
            </Modal>
        );
    }
    renderTable() {
        const getDeviceCreatedDate = date => {
            if (typeof date === "string") return date;
            return date.toISOString();
        };
        return this.props.devices.map((item, index) => (
            <tr key={index}>
                <td>{(index + 1)}.</td>
                <td>{item.name}</td>
                <td>{item.ip}</td>
                <td>{getDeviceCreatedDate(item.addedAt)}</td>
                <td>
                    {
                        this.props.canEdit ?
                            <ButtonToolbar>
                                <Button bsSize="xsmall" bsStyle="info" onClick={() => this.setState({ modal: true, selectedDeviceId: item._id })}>Zmień nazwę</Button>
                                <Button bsSize="xsmall" bsStyle="danger" onClick={this.removeDevice.bind(null, item._id)}>Usuń</Button>
                            </ButtonToolbar>
                            :
                            null
                    }
                </td>
            </tr>
        ));
    }
    render() {
        return (
            <div>
                {this.renderModal()}
                <h1>Lista urządzeń</h1>
                <Alert bsStyle="warning">
                    <strong>Informacja!</strong> Aby dane z urządzenia wyświetały się na wykresach, urządzenie musi znajdować się na poniższej liście z poprawnym adresem IP urządzenia.
                </Alert>
                <Alert bsStyle="info">
                    <strong>Informacja!</strong> Nierozpoznane urządzenia zostaną automatycznie dodane do tej listy, po przesłaniu pierwszego pakietu danych z danego adresu IP.
                </Alert>
                <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        <th>#.</th>
                        <th>Nazwa</th>
                        <th>Adres IP</th>
                        <th>Dodano (data)</th>
                        <th>Akcje</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderTable()}
                    </tbody>
                </Table>
            </div>
        );
    }
}


export default withTracker(() => {
    const canEdit = (Meteor.user() && Meteor.user().role === 'admin');
    return {
        devices: DevicesCollection.find().fetch(),
        canEdit
    }
})(Devices);
