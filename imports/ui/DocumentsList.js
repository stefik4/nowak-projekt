import React from 'react';
import ReactTable from 'react-table'
import { withTracker } from 'meteor/react-meteor-data';

import Data from './../api/collections/data';

import 'react-table/react-table.css'

class DocumentsList extends React.Component {
    render() {
        const columns = [
            {
                Header: 'Data pomiaru',
                accessor: 'date',
                Cell: props => <span>{props.value}</span>
            },
            {
                Header: 'Typ',
                accessor: 'type',
            },
            {
                Header: 'Wartość',
                accessor: 'value',
            },
            {
                Header: 'Źródło danych',
                accessor: 'source'
            }
        ];
        return (
            <div>
                <h1>Dokumenty</h1>
                <ReactTable
                    data={this.props.data}
                    columns={columns}
                />
            </div>
        );
    }
}


export default withTracker(() => {
    return {
        data: Data.find().fetch()
    }
})(DocumentsList);
