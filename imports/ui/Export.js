import React from 'react';
import XLSX from 'xlsx';
import { ButtonToolbar, Button } from 'react-bootstrap';
import { withTracker } from 'meteor/react-meteor-data';

import DataTypes from "../api/collections/dataTypes";
import Devices from "../api/collections/devices";

class Export extends React.Component {
    constructor() {
        super();
        this.formats = [
            {
                ext: 'csv',
                bsStyle: 'primary'
            },
            {
                ext: 'xls',
                bsStyle: 'info'
            },
            {
                ext: 'xlsx',
                bsStyle: 'warning'
            }
        ];
    }
    export(selector, format) {
        Meteor.call('download', selector, (err, result) => {
            if (err) throw err;
            XLSX.writeFile(result, `export_${new Date().toISOString()}.${format}`);
        });
    }
    render() {
        return (
            <div>
                <h1>Export danych</h1>
                <h3>Wszystkie dane</h3>
                <ButtonToolbar>
                {
                    this.formats.map((item, key) => (
                        <Button
                            key={key}
                            bsStyle={item.bsStyle}
                            onClick={this.export.bind(this, {}, item.ext)}>
                            Pobierz dane (*.{item.ext})
                        </Button>
                    ))
                }
                </ButtonToolbar>
                <h3>Dane filtrowane według typów</h3>
                {
                    this.props.dataTypes.map((element, key) => {
                        return (
                            <div key={key}>
                                <h4>{element.name} (klucz: {element.key})</h4>
                                <ButtonToolbar>
                                {
                                    this.formats.map((item, key) => (
                                        <Button
                                            key={key}
                                            bsStyle={item.bsStyle}
                                            onClick={this.export.bind(this, { type: element.key }, item.ext)}>
                                            Pobierz dane (*.{item.ext})
                                        </Button>
                                    ))
                                }
                                </ButtonToolbar>
                            </div>
                        )
                    })
                }
                <h3>Dane filtrowane według urządzeń</h3>
                {
                    this.props.devices.map((element, key) => {
                        return (
                            <div key={key}>
                                <h4>{element.name} (IP: {element.ip})</h4>
                                <ButtonToolbar>
                                {
                                    this.formats.map((item, key) => (
                                        <Button
                                            key={key}
                                            bsStyle={item.bsStyle}
                                            onClick={this.export.bind(this, { source: element.ip }, item.ext)}>
                                            Pobierz dane (*.{item.ext})
                                        </Button>
                                    ))
                                }
                                </ButtonToolbar>
                            </div>
                        )
                    })
                }
            </div>
        );
    }
}


export default withTracker(() => {
    return {
        dataTypes: DataTypes.find({}, { fields: { name: 1, key: 1 } }).fetch(),
        devices: Devices.find({}, { fields: { name: 1, ip: 1 } }).fetch()
    }
})(Export);
