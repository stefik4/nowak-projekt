import React from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import { withTracker } from 'meteor/react-meteor-data';
import AccountsUIWrapper from './AccountsUIWrapper';
import UserLoginStatus from './UserLoginStatus';
import Devices from './Devices';
import DocumentsList from './DocumentsList';
import Export from './Export';
import Charts from './Charts';
import Statistics from './Statistics';
import DataTypes from './DataTypes';

class HomePage extends React.Component {
    render() {
        return (
            <div style={{ marginBottom: '3em' }}>
                <AccountsUIWrapper />
                <UserLoginStatus />
                {
                    this.props.isLogged ?
                        <Tabs defaultActiveKey={1} animation={true} id="tabs">
                            <Tab eventKey={1} title="Wykresy wartości">
                                <Charts/>
                            </Tab>
                            <Tab eventKey={2} title="Statystyki">
                                <Statistics/>
                            </Tab>
                            <Tab eventKey={3} title="Przeglądanie danych">
                                <DocumentsList />
                            </Tab>
                            <Tab eventKey={4} title="Export danych">
                                <Export/>
                            </Tab>
                            <Tab eventKey={5} title="Urządzenia">
                                <Devices/>
                            </Tab>
                            <Tab eventKey={6} title="Typy danych">
                                <DataTypes/>
                            </Tab>
                        </Tabs>
                        :
                        null
                }
            </div>
        );
    }
}

export default withTracker(() => {
    return {
        isLogged: !!Meteor.user()
    }
})(HomePage);
