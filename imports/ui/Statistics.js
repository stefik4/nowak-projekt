import React from 'react';
import { ListGroup, ListGroupItem, Modal, Button, Table, Badge } from 'react-bootstrap';
import { withTracker } from 'meteor/react-meteor-data';

import Data from './../api/collections/data';
import DataTypes from './../api/collections/dataTypes';

class Statistics extends React.Component {
    constructor() {
        super();
        this.state = {
            modal: false,
            name: '',
            min: [],
            max: []
        };
        this.showDetails = this.showDetails.bind(this);
    }
    showDetails(data) {
        this.setState({
            modal: true,
            name: data.item.name,
            max: [
                {
                    text: 'Data pomiaru',
                    value: data.max.date || ''
                },
                {
                    text: 'Wartość',
                    value: data.max.value || ''
                },
                {
                    text: 'Źródło',
                    value: data.max.source || ''
                }
            ],
            min: [
                {
                    text: 'Data pomiaru',
                    value: data.min.date || ''
                },
                {
                    text: 'Wartość',
                    value: data.min.value || ''
                },
                {
                    text: 'Źródło',
                    value: data.min.source || ''
                }
            ]
        });
    }

    renderModal() {
        return (
            <Modal show={this.state.modal}>
                <Modal.Header>
                    <Modal.Title>Szczegóły pomiaru - {this.state.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Table striped bordered condensed hover>
                        <tbody>
                        <tr>
                            <th colSpan={2}>Pomiar minimalny</th>
                        </tr>
                        {this.state.min.map((val, key) => (
                            <tr key={key}>
                                <td>{val.text}</td>
                                <td>{val.value}</td>
                            </tr>
                        ))}
                        <tr>
                            <th colSpan={2}>Pomiar maksymalny</th>
                        </tr>
                        {this.state.max.map((val, key) => (
                            <tr key={key}>
                                <td>{val.text}</td>
                                <td>{val.value}</td>
                            </tr>
                        ))}
                        </tbody>
                    </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        bsStyle="primary"
                        onClick={() => this.setState({ modal: false })}
                    >Zamknij</Button>
                </Modal.Footer>
            </Modal>
        );
    }
    render() {
        if(!this.props.ready) return null;
        return (
            <div>
                <h1>Statystyki</h1>
                {this.renderModal()}
                <ListGroup>
                    {this.props.data.map((element, key) => {
                        if (element && element.visible) {
                            return (
                                <ListGroupItem
                                    key={key}
                                    header={element.name}
                                    onClick={element.details ? this.showDetails.bind(null, element.details) : null}
                                >
                                    Wartość maksymalna: <Badge>{element.max}</Badge> | Wartość minimalna: <Badge>{element.min}</Badge>
                                </ListGroupItem>
                            );
                        }
                    })}
                </ListGroup>
            </div>
        );
    }
}


export default withTracker(() => {
    const ready = !(Data.find().count() === 0);
    const data = DataTypes.find().map(item => {
        const { name } = item;
        const max = Data.findOne({ type: item.key }, { sort: { value: -1 } });
        const min = Data.findOne({ type: item.key }, { sort: { value: 1 } });
        return {
            visible: (max && min),
            name,
            max: max ? max.value : null,
            min: min ? min.value : null,
            details: {
                item,
                max,
                min
            }
        };
    });
    return {
        ready,
        data
    }
})(Statistics);
