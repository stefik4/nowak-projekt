import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Alert } from 'react-bootstrap';

class UserLoginStatus extends React.Component {
    render() {
        if (this.props.user) {
            return (
                <Alert bsStyle="success">
                Witaj <strong>{this.props.user.username}</strong>. Rodzaj konta: <strong>{this.props.user.role || 'user'}</strong>
        </Alert>
            );
        }
        return (
            <Alert bsStyle="danger">
                <strong>Korzystanie z serwisu wymaga logowania.</strong>
            </Alert>
        );
    }
}


export default withTracker(() => {
    return {
        user: Meteor.user()
    }
})(UserLoginStatus);
