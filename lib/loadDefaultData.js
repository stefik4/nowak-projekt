import Devices from "/imports/api/collections/devices";
import Data from "/imports/api/collections/data";
import DataTypes from "/imports/api/collections/dataTypes";

export const loadDefaultData = () => {
    const isLoadNeeded = true;
    // Adding fake devices
    if (isLoadNeeded && Devices.find().count() === 0) {
        for (let i = 1; i <= 5; i += 1) {
            const device = Devices.insert({
                name: 'Urządzenie nr ' + i,
                ip: '192.168.4.' + i,
                addedAt: new Date(),
                addedBy: null
            });
            console.log('Added fake device with id:', device);
        }
    }

    // Adding data types
    if (isLoadNeeded && DataTypes.find().count() === 0) {
        DataTypes.insert({
            key: 'temp',
            name: 'Temperatura'
        });
        DataTypes.insert({
            key: 'wilg',
            name: 'Wilgotność'
        });
        DataTypes.insert({
            key: 'swiatlo',
            name: 'Natężenie światła'
        });

    }

    // Adding fake data
    if (isLoadNeeded && Data.find().count() === 0 && false) {
        for (let i = 0; i < 50; i += 1) {
            const getRandom = (min, max) => {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            };
            const ms = new Date().getTime() - getRandom(8000000,99999999);
            const date = new Date(ms);
            Data.insert({
                date: date.toISOString(),
                type: 'temp',
                value: getRandom(18,24),
                source: '192.168.4.' + getRandom(1,5)
            });
            Data.insert({
                date: date.toISOString(),
                type: 'wilg',
                value: getRandom(55,97),
                source: '192.168.4.' + getRandom(1,5)
            });
            Data.insert({
                date: date.toISOString(),
                type: 'swiatlo',
                value: getRandom(239,656),
                source: '192.168.4.' + getRandom(1,5)
            });
        }
    }

    // Adding users
    if (Meteor.users.find().count() === 0) {
        const admin = Accounts.createUser({
            username: 'admin',
            password: 'admin'
        });
        Meteor.users.update(admin, { $set: { role: 'admin' } });
        console.info('Created admin with id:', admin);
        const user = Accounts.createUser({
            username: 'user',
            password: 'user'
        });
        Meteor.users.update(user, { $set: { role: 'user' } });
        console.info('Created user with id:', user);
    }
};
