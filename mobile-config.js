App.info({
    id: 'com.putpoznan.iot',
    name: 'IoT',
    description: 'IoT Project',
    author: 'Jakub Stephan',
    email: 'stefik4@wp.pl'
});

App.setPreference('LoadUrlTimeoutValue', 60000);
App.setPreference('WebAppStartupTimeout', 60000);
App.setPreference('ShowUpdateScreenSpinner', true);
App.setPreference('SplashShowOnlyFirstTime', false);
