import '../imports/api/index';
import { loadDefaultData } from '/lib/loadDefaultData';

Meteor.startup(() => {
    // Load default data
    loadDefaultData();
});


